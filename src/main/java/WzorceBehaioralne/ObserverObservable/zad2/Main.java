package WzorceBehaioralne.ObserverObservable.zad2;

import java.util.Observer;

public class Main {
    public static void main(String[] args) {

        ChatRoom znajomi = new ChatRoom("Znajomi");
        ChatRoom czatPraca = new ChatRoom("Praca");
        ChatRoom kurs = new ChatRoom("Kurs");
        InternetPortal portal = new InternetPortal();
        portal.addRoom(znajomi);
        portal.addRoom(czatPraca);
        portal.addRoom(kurs);
        User Iwona = new User("Iwona");
        User Kamila = new User("Kamila");
        User Julka = new User("Julka");
        User Kasia = new User("Kasia");
        User Monika = new User("Monika");
        portal.addUserToRoom(Iwona,czatPraca);
        portal.addUserToRoom(Kamila,czatPraca);
        portal.addUserToRoom(Julka,czatPraca);
        portal.addUserToRoom(Kasia,czatPraca);
        portal.addUserToRoom(Monika,czatPraca);
        portal.addUserToRoom(Iwona,znajomi);
        System.out.println(portal.wylistujPokoje());
        portal.sendMessageToRoom("Bez Iwony",czatPraca);
        System.out.println();
        portal.sendMessageToRoom("Tylko Iwona",znajomi);

    }
}
