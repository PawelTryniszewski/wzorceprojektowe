package WzorceBehaioralne.ObserverObservable.zad2;

import java.util.*;

public class InternetPortal {
    List<ChatRoom> chatRoomList = new ArrayList<>();

    public void addRoom(ChatRoom chatRoom) {
        chatRoomList.add(chatRoom);
    }

    public void removeRoom(ChatRoom chatRoom) {
        chatRoomList.remove(chatRoom);
    }

    public List<ChatRoom> wylistujPokoje() {
        return chatRoomList;
    }

    public void addUserToRoom(User user, ChatRoom chatRoom) {
        for (ChatRoom ch:chatRoomList) {
            ch.deleteObserver(user);
        }
        chatRoom.addObserver(user);

    }

    public void sendMessageToRoom(String string, ChatRoom chatRoom) {
        chatRoom.wyslijWiadomosc(string);
    }

}
