package WzorceBehaioralne.ObserverObservable.zad2;

import java.util.Observable;
import java.util.Observer;

public class User implements Observer {
    String imie;

    @Override
    public String toString() {
        return "User "+ imie;
    }

    public User(String imie) {
        this.imie = imie;
    }

    @Override
    public void update(Observable o, Object arg) {

        //if (arg instanceof ChatRoom){
            System.out.println(imie+" "+arg);
       // }

    }
}
