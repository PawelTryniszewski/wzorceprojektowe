package WzorceBehaioralne.ObserverObservable.zad2;

import java.util.Observable;

public class ChatRoom extends Observable {
    String nazwaChatu;

    @Override
    public String toString() {
        return "Chat " + nazwaChatu ;
    }

    public ChatRoom(String nazwaChatu) {
        this.nazwaChatu = nazwaChatu;
    }

    public void wyslijWiadomosc(String wiadomosc){
        setChanged();
        notifyObservers(wiadomosc);

    }


}
