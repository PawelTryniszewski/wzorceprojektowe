package WzorceBehaioralne.ObserverObservable.zad3;

public class Main {
    public static void main(String[] args) {


        PlikKońcowy wiadomosci = new PlikKońcowy();
        PlikKońcowy info = new PlikKońcowy();
        PlikKońcowy news = new PlikKońcowy();
        Rozsylacz.getInstance().addObserver(wiadomosci);
        Rozsylacz.getInstance().addObserver(info);
        Rozsylacz.getInstance().addObserver(news);
        Rozsylacz.getInstance().dodaj(wiadomosci,"wiadomosci.txt");
        Rozsylacz.getInstance().dodaj(info,"info.txt");
        Rozsylacz.getInstance().dodaj(news,"news.txt");
        Rozsylacz.getInstance().wyslijWiadomosc("blalalal2");
        Rozsylacz.getInstance().wyslijWiadomosc("blalalal2");
        Rozsylacz.getInstance().wyslijWiadomosc("blalalal2");

    }

}
