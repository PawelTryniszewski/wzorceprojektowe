package WzorceBehaioralne.ObserverObservable.zad3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Observable;
import java.util.Observer;

public class PlikKońcowy implements Observer {

    String path;

    public void setPathOrCreateNewFile(String path) {
        if (path != null) {
            this.path = path;
        }else{
            this.path = "abc.txt";
        }
    }
    public void deleteFile(PrintWriter writer){

    }

    @Override
    public void update(Observable o, Object arg) {

        try (PrintWriter writer = new PrintWriter(new FileWriter(path, true))) {
            writer.println(arg);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
