package WzorceBehaioralne.ObserverObservable.zad3;

import java.util.Observable;

public class Rozsylacz extends Observable {
    private final static Rozsylacz instance = new Rozsylacz();
    public static Rozsylacz getInstance(){
        return instance;
    }

    private Rozsylacz() {
    }

    public void wyslijWiadomosc(String wiadomosc){
        setChanged();
        notifyObservers(wiadomosc);

    }
    public void dodaj(PlikKońcowy plikKońcowy,String s){
        plikKońcowy.setPathOrCreateNewFile(s);
    }
    public void usun(PlikKońcowy plikKońcowy){
        plikKońcowy.setPathOrCreateNewFile(null);
        deleteObserver(plikKońcowy);
    }

    public void wyslij(PlikKońcowy plikKońcowy,String s){
        wyslijWiadomosc(s);
    }
}
