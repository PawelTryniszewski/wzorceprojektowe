package WzorceBehaioralne.ObserverObservable.zad1;

public class Wiadomosc {
    String tresc;
    int wagaWiadomosci;

    public int getWagaWiadomosci() {
        return wagaWiadomosci;
    }

    @Override
    public String toString() {
        return  tresc;
    }

    public Wiadomosc(String tresc, int wagaWiadomosci) {
        this.tresc = tresc;
        this.wagaWiadomosci = wagaWiadomosci;
    }
}
