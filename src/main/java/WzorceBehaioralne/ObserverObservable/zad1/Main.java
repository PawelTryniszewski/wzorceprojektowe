package WzorceBehaioralne.ObserverObservable.zad1;

public class Main {
    public static void main(String[] args) {
        NewsStation newsStation = new NewsStation();
        newsStation.addObserver(new Watcher(3,"Adam"));
        newsStation.addObserver(new Watcher(4,"Dawid"));
        newsStation.addObserver(new Watcher(7,"Piort"));
        newsStation.addObserver(new Watcher(3,"Kasia"));
        newsStation.addObserver(new Watcher(3,"Basia"));

       // newsStation.powiadom(new Wiadomosc("Deszcz",4));
        newsStation.powiadom(new MarketingNews());

    }
}
