package WzorceBehaioralne.ObserverObservable.zad1;


import java.util.Observable;

public class NewsStation extends Observable {

    public void powiadom(Wiadomosc wiadomosc){
        setChanged();
        notifyObservers(wiadomosc);
    }

    public void powiadom(MarketingNews marketingNews){
        setChanged();
        notifyObservers(marketingNews);
    }




}
