package WzorceBehaioralne.ObserverObservable.zad1;

public class Product {
    String nazwa;
    int cena;

    @Override
    public String toString() {
        return  nazwa +" "+ cena;
    }

    public Product(String nazwa, int cena) {
        this.nazwa = nazwa;
        this.cena = cena;
    }
}
