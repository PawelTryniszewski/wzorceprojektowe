package WzorceBehaioralne.ObserverObservable.zad1;

import java.util.Observable;
import java.util.Observer;
import java.util.Random;

public class Watcher implements Observer {
    int poziomPAniki;
    String imie;
    Random r = new Random();

    public Watcher(int poziomPAniki, String imie) {
        this.poziomPAniki = poziomPAniki;
        this.imie = imie;
    }

    @Override
    public void update(Observable o, Object arg) {

        if (arg instanceof Wiadomosc) {

            if (poziomPAniki < ((Wiadomosc) arg).getWagaWiadomosci()) {
                System.out.println(imie + " Został powiadomiony o :" + arg);
            } else {
                for (int i = 0; i < 10; i++) {

                    System.out.println(imie + " Olaboga ");
                    try {
                        Thread.sleep(800);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (arg instanceof MarketingNews) {
            MarketingNews news = ((MarketingNews)arg);
            Random rn = new Random();
            for (Product p : news.listaProduktów ) {
                if(rn.nextBoolean()){
                    if(rn.nextInt(100)> 15) {
                        System.out.println(imie+" Jestem zainteresowany: " + p);
                    }
                }
            }
        }

    }
}

