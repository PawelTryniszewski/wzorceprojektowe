package WzorceBehaioralne.ObserverObservableNowy.zad1;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class Watcher implements ChangeListener<Wiadomosc> {
    String imie;
    int poziomPaniki;

    public Watcher(String imie, int poziomPaniki) {
        this.imie = imie;
        this.poziomPaniki = poziomPaniki;
    }


    @Override
    public void changed(ObservableValue<? extends Wiadomosc> observable, Wiadomosc oldValue, Wiadomosc newValue) {
        System.out.println(newValue);
    }
}
