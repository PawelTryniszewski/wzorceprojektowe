package WzorceBehaioralne.ObserverObservableNowy.zad1;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;


public class NewsStation {
    private ObjectProperty<Wiadomosc> wiadomoscObjectProperty = new SimpleObjectProperty<>();

    public void addObserver(Watcher stringChangeListener){
        wiadomoscObjectProperty.addListener((ChangeListener<Wiadomosc>) stringChangeListener);
    }

    public void wyslijWiadomosc(Wiadomosc wiadomosc){
        wiadomoscObjectProperty.setValue(wiadomosc);
    }
}
