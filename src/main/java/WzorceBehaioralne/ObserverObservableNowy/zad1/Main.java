package WzorceBehaioralne.ObserverObservableNowy.zad1;

public class Main {
    public static void main(String[] args) {
        NewsStation nowy = new NewsStation();
        nowy.addObserver(new Watcher("Pawel",10));
        nowy.wyslijWiadomosc(new Wiadomosc("Deszcz",12));
    }
}
