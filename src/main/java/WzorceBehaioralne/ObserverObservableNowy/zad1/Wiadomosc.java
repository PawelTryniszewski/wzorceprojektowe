package WzorceBehaioralne.ObserverObservableNowy.zad1;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class Wiadomosc  {
    String tresc;
    int wagaWiadomosci;

    public int getWagaWiadomosci() {
        return wagaWiadomosci;
    }

    @Override
    public String toString() {
        return  tresc;
    }

    public Wiadomosc(String tresc, int wagaWiadomosci) {
        this.tresc = tresc;
        this.wagaWiadomosci = wagaWiadomosci;
    }


}
