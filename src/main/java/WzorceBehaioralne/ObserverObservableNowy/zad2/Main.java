package WzorceBehaioralne.ObserverObservableNowy.zad2;

public class Main {
    public static void main(String[] args) {

        ChatRoom chatRoom = new ChatRoom();
        chatRoom.addObserver(new User("Pawel"));
        chatRoom.addObserver(new User("Adam"));
        chatRoom.addObserver(new User("Slawek"));
        chatRoom.wyslijWiadomosc("Nowa Wiadomość.");
    }

}
