package WzorceBehaioralne.ObserverObservableNowy.zad2;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class User implements ChangeListener<String> {
    String name;

    public User(String name) {
        this.name = name;
    }


    @Override
    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        System.out.println(name+" Otrzymal info o : "+newValue);
    }
}
