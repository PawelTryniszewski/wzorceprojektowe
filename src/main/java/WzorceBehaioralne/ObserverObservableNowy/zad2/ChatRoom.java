package WzorceBehaioralne.ObserverObservableNowy.zad2;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;

public class ChatRoom {
    private StringProperty stringProperty = new SimpleStringProperty();

    public void addObserver(ChangeListener<String> stringChangeListener){
        stringProperty.addListener( stringChangeListener);
    }

    public void wyslijWiadomosc(String wiadomosc){
        stringProperty.setValue(wiadomosc);
    }
}
