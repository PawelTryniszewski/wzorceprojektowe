package WzorceStworzeniowe.Singleton;

public class Game {
    private int counter;
    private int score;

    private Round round;

    /**
     * Zwraca true / false w zaleznosci od tego czy userResult jest rowne
     * poprawnemu wynikowi.
     *
     * @param userResult
     */
    public boolean validate(int userResult) {
        boolean result = round.validate(userResult);

        if (result) {
            score++;
        }
        return result;
    }

    /**
     * Metoda ktora przechodzi do nastepnej rundy i generuje dwie kolejne liczby.
     */
    public void nextRound() {
        // kolejna runda zwiększa licznik
        counter++;

        // generuje liczby
        round = new Round();
        System.out.println(round);
    }

    /**
     * Czy gra sie skonczyla.
     */
    public boolean hasEnded() {
        return counter >= Settings.INSTANCE.getNumberOfRounds();
    }


}
