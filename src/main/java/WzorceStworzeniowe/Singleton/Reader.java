package WzorceStworzeniowe.Singleton;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Reader {


    public void readText() {
        File odczytPliku = new File("gra.txt");
        try {
            Scanner scanner = new Scanner((odczytPliku));


            String tekst1 = scanner.nextLine();
            String tekst2 = scanner.nextLine();
            String tekst3 = scanner.nextLine();
            String tekst4 = scanner.nextLine();

            String value1 = tekst1.split("=")[1];
            String value2 = tekst2.split("=")[1];
            String value3 = tekst3.split("=")[1];
            String value4 = tekst4.split("=")[1];

            int range1= Integer.parseInt(value1);
            int range2= Integer.parseInt(value2);
            int rounds = Integer.parseInt(value4);


            Settings.INSTANCE.setRange1(range1);
            Settings.INSTANCE.setRange2(range2);
            Settings.INSTANCE.setNumberOfRounds(rounds);
            Settings.INSTANCE.setSigns(value3);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
