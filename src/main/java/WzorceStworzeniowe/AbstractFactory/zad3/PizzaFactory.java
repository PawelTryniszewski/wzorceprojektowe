package WzorceStworzeniowe.AbstractFactory.zad3;

public abstract class PizzaFactory {
    public static Pizza stworzMargarrite() {
        Pizza pizza = new Pizza("Margarritta");

        pizza.getSkladniki().add("Ser");
        pizza.getSkladniki().add("Sos");
        pizza.getSkladniki().add("Bazylia");

        return pizza;
    }

    public static Pizza stworzHawajska() {
        Pizza pizza = new Pizza("Hawajska");

        pizza.getSkladniki().add("Ser");
        pizza.getSkladniki().add("Sos");
        pizza.getSkladniki().add("Bazylia");
        pizza.getSkladniki().add("Ananas");
        pizza.getSkladniki().add("Szynka");

        return pizza;
    }

    public static Pizza stworzWiejska() {
        Pizza pizza = new Pizza("Wiejska");

        pizza.getSkladniki().add("Ser");
        pizza.getSkladniki().add("Sos");
        pizza.getSkladniki().add("Bazylia");
        pizza.getSkladniki().add("Kiełbasa");
        pizza.getSkladniki().add("Cebula");
        pizza.getSkladniki().add("Boczek");

        return pizza;
    }

    public static Pizza stworzCzterySery(){
        return new Pizza.PizzaBuilder()
                .setNazwa("4Sery")
                .dodajSkladnik("Ser")
                .dodajSkladnik("Sos")
                .dodajSkladnik("Parmezan")
                .dodajSkladnik("Mozarella")
                .dodajSkladnik("Gouda")
                .dodajSkladnik("Ser z biedronki")
                .stworz();
    }

}
