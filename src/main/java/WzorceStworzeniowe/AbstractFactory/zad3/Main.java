package WzorceStworzeniowe.AbstractFactory.zad3;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        Set<Pizza> pizzas = new HashSet<>();

        pizzas.add(PizzaFactory.stworzCzterySery());
        pizzas.add(PizzaFactory.stworzHawajska());
//        pizzas.add(PizzaFactory.stworzHawajska());
//        pizzas.add(PizzaFactory.stworzHawajska());
        pizzas.add(PizzaFactory.stworzMargarrite());
        pizzas.add(PizzaFactory.stworzWiejska());

        System.out.println(pizzas);
        System.out.println(pizzas.size());

        System.out.println(PizzaFactory.stworzHawajska().getSkladniki());
    }
}
