package WzorceStworzeniowe.AbstractFactory.zad2;

public class Bike  {
    String nazwa;
    int iloscMiejsc,iloscPrzerzutek;
    BIKE_TYPE typ;

    @Override
    public String toString() {
        return "Bike{" +
                "nazwa='" + nazwa + '\'' +
                ", iloscMiejsc=" + iloscMiejsc +
                ", iloscPrzerzutek=" + iloscPrzerzutek +
                ", typ=" + typ +
                '}';
    }

    public Bike(String nazwa, int iloscMiejsc, int iloscPrzerzutek, BIKE_TYPE typ) {
        this.nazwa = nazwa;
        this.iloscMiejsc = iloscMiejsc;
        this.iloscPrzerzutek = iloscPrzerzutek;
        this.typ = typ;
    }
}
