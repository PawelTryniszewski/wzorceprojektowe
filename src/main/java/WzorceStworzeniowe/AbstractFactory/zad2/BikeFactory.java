package WzorceStworzeniowe.AbstractFactory.zad2;

public abstract class BikeFactory {

    public static Bike stworzKross(){
        return new Bike("Kross",1,5,BIKE_TYPE.BICYCLE);
    }
    public static Bike stworzMerida(){
        return new Bike("Merida",1,6,BIKE_TYPE.BICYCLE);
    }
    public static Bike stworzIniana(){
        return new Bike("Iniana",2,3,BIKE_TYPE.TANDEM);
    }
    public static Bike stworzGoetze(){
        return new Bike("Goetze",2,1,BIKE_TYPE.TANDEM);
    }

    public static Bike stworzFelt(){
        return new Bike("Felt",1,6,BIKE_TYPE.BICYCLE);
    }

}
