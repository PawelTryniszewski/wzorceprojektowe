package WzorceStworzeniowe.AbstractFactory.zad4;

public abstract class AppleFactory {
    public static Telefon stworzIphoX(){
        return new Telefon.Builder().setIloscKartSim(1).setIloscRdzeni(8).setMocProcesora(1000000).setModel("X")
                .setProducent("Apple").setSzerokoscEkranu(4).setWysokoscEkranu(5).createTelefon();
    }
}
