package WzorceStworzeniowe.AbstractFactory.zad4;

public class Main {
    public static void main(String[] args) {
        Telefon nokia = new Telefon.Builder().setIloscKartSim(1).createTelefon();
        System.out.println(SamsungFactory.stworzSamsungNote());
        System.out.println(AppleFactory.stworzIphoX());
    }
}
