package WzorceStworzeniowe.AbstractFactory.zad4;

public abstract class SamsungFactory {

    public static Telefon stworzSamsungNote(){
        return new Telefon.Builder().setIloscKartSim(1).setIloscRdzeni(8).setMocProcesora(1000000).setModel("Note")
                .setProducent("Samsung").setSzerokoscEkranu(4).setWysokoscEkranu(5).createTelefon();
    }
}
