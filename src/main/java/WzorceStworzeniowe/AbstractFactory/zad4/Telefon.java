package WzorceStworzeniowe.AbstractFactory.zad4;

public class Telefon {
   String producent, model;
    int szerokoscEkranu, wysokoscEkranu, mocProcesora, iloscRdzeni, iloscKartSim;

    @Override
    public String toString() {
        return "Telefon{" +
                "producent='" + producent + '\'' +
                ", model='" + model + '\'' +
                ", szerokoscEkranu=" + szerokoscEkranu +
                ", wysokoscEkranu=" + wysokoscEkranu +
                ", mocProcesora=" + mocProcesora +
                ", iloscRdzeni=" + iloscRdzeni +
                ", iloscKartSim=" + iloscKartSim +
                '}';
    }

    public Telefon(String producent, String model, int szerokoscEkranu,
                   int wysokoscEkranu, int mocProcesora, int iloscRdzeni, int iloscKartSim) {
        this.producent = producent;
        this.model = model;
        this.szerokoscEkranu = szerokoscEkranu;
        this.wysokoscEkranu = wysokoscEkranu;
        this.mocProcesora = mocProcesora;
        this.iloscRdzeni = iloscRdzeni;
        this.iloscKartSim = iloscKartSim;
    }
    public static class Builder{
        private String producent;
        private String model;
        private int szerokoscEkranu;
        private int wysokoscEkranu;
        private int mocProcesora;
        private int iloscRdzeni;
        private int iloscKartSim;

        public Builder setProducent(String producent) {
            this.producent = producent;
            return this;
        }

        public Builder setModel(String model) {
            this.model = model;
            return this;
        }

        public Builder setSzerokoscEkranu(int szerokoscEkranu) {
            this.szerokoscEkranu = szerokoscEkranu;
            return this;
        }

        public Builder setWysokoscEkranu(int wysokoscEkranu) {
            this.wysokoscEkranu = wysokoscEkranu;
            return this;
        }

        public Builder setMocProcesora(int mocProcesora) {
            this.mocProcesora = mocProcesora;
            return this;
        }

        public Builder setIloscRdzeni(int iloscRdzeni) {
            this.iloscRdzeni = iloscRdzeni;
            return this;
        }

        public Builder setIloscKartSim(int iloscKartSim) {
            this.iloscKartSim = iloscKartSim;
            return this;
        }

        public Telefon createTelefon() {
            return new Telefon(producent, model, szerokoscEkranu, wysokoscEkranu, mocProcesora, iloscRdzeni, iloscKartSim);
        }

        @Override
        public String toString() {
            return "Builder{" +
                    "producent='" + producent + '\'' +
                    ", model='" + model + '\'' +
                    ", szerokoscEkranu=" + szerokoscEkranu +
                    ", wysokoscEkranu=" + wysokoscEkranu +
                    ", mocProcesora=" + mocProcesora +
                    ", iloscRdzeni=" + iloscRdzeni +
                    ", iloscKartSim=" + iloscKartSim +
                    '}';
        }
    }

}
