package WzorceStworzeniowe.AbstractFactory.zad1;

public class AsusPC extends AbstractPc {
    public AsusPC(String nazwaKomputera, COMPUTER_BRAND markaKomputera, int cpu_power, double gpu_power, boolean isOverclocked) {
        super(nazwaKomputera, markaKomputera, cpu_power, gpu_power, isOverclocked);
    }



    public static AsusPC stworzAsus() {
        return new AsusPC("Asus",COMPUTER_BRAND.ASUS,90,0.10,false) {
        };
    }
    public static AsusPC stworzAsus1() {
        return new AsusPC("Asus1",COMPUTER_BRAND.ASUS,50,0.40,true) {
        };
    }

}
