package WzorceStworzeniowe.AbstractFactory.zad1;

public class SamsungPC extends AbstractPc {
    private SamsungPC(String nazwaKomputera, COMPUTER_BRAND markaKomputera, int cpu_power, double gpu_power, boolean isOverclocked) {
        super(nazwaKomputera, markaKomputera, cpu_power, gpu_power, isOverclocked);
    }

    public static SamsungPC stworzSamsung1(){
        return new SamsungPC("Samsung1", COMPUTER_BRAND.SAMSUNG,100,1.00,true);
    }
    public static SamsungPC stworzSamsung2(){
        return new SamsungPC("Samsung2", COMPUTER_BRAND.SAMSUNG,100,1.00,false);
    }
}
