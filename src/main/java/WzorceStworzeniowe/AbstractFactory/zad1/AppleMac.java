package WzorceStworzeniowe.AbstractFactory.zad1;

public class AppleMac extends AbstractPc {
    private AppleMac(String nazwaKomputera, COMPUTER_BRAND markaKomputera, int cpu_power, double gpu_power, boolean isOverclocked) {
        super(nazwaKomputera, markaKomputera, cpu_power, gpu_power, isOverclocked);
    }

    public static AppleMac stworzMaca1(){
        return new AppleMac("MacBook1",COMPUTER_BRAND.APPLE,7,0.01,false);
    }


    public static AppleMac stworzMaca2(){
        return new AppleMac("MacBook2",COMPUTER_BRAND.APPLE,2,0.01,false);
    }
}
