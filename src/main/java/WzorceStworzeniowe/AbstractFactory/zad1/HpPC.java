package WzorceStworzeniowe.AbstractFactory.zad1;

public class HpPC extends AbstractPc {
    private HpPC(String nazwaKomputera, COMPUTER_BRAND markaKomputera, int cpu_power, double gpu_power, boolean isOverclocked) {
        super(nazwaKomputera, markaKomputera, cpu_power, gpu_power, isOverclocked);
    }

    public static HpPC stworzHp1(){
        return new HpPC("Hp1",COMPUTER_BRAND.HP,88,0.44,true);
    }
    public static HpPC stworzHp2(){
        return new HpPC("Hp2",COMPUTER_BRAND.HP,88,0.99,false);
    }
}
