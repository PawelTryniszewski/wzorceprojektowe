package WzorceStworzeniowe.AbstractFactory.zad1;

public abstract class AbstractPc {
    private String nazwaKomputera;
    private COMPUTER_BRAND markaKomputera;
    private int cpu_power;//(int 0-100)
    private double gpu_power;// (double 0.00 - 1.00)
    private boolean isOverclocked;

    public AbstractPc() {

    }

    public AbstractPc(String nazwaKomputera, COMPUTER_BRAND markaKomputera, int cpu_power, double gpu_power, boolean isOverclocked) {
        this.nazwaKomputera = nazwaKomputera;
        this.markaKomputera = markaKomputera;
        this.cpu_power = cpu_power;
        this.gpu_power = gpu_power;
        this.isOverclocked = isOverclocked;
    }

    public String getNazwaKomputera() {
        return nazwaKomputera;
    }

    public void setNazwaKomputera(String nazwaKomputera) {
        this.nazwaKomputera = nazwaKomputera;
    }

    public COMPUTER_BRAND getMarkaKomputera() {
        return markaKomputera;
    }

    public void setMarkaKomputera(COMPUTER_BRAND markaKomputera) {
        this.markaKomputera = markaKomputera;
    }

    public int getCpu_power() {
        return cpu_power;
    }

    public void setCpu_power(int cpu_power) {
        this.cpu_power = cpu_power;
    }

    public double getGpu_power() {
        return gpu_power;
    }

    public void setGpu_power(double gpu_power) {
        this.gpu_power = gpu_power;
    }

    public boolean isOverclocked() {
        return isOverclocked;
    }

    public void setOverclocked(boolean overclocked) {
        isOverclocked = overclocked;
    }
}
