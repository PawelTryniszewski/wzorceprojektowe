package WzorceStworzeniowe.Strategia;

public class ToLowerFormatter implements IFormatter {
    @Override
    public String format(String text) {
        return text.toLowerCase();
    }
}
