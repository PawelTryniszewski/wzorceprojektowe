package WzorceStworzeniowe.Strategia;

public class CapitalFormatter implements IFormatter {
    @Override
    public String format(String text) {
        String[] temp = text.split("\\s+");

        for (int i = 0; i < temp.length; ++i) {
            temp[i]=String.valueOf(temp[i].toCharArray()[0]).toUpperCase()+temp[i].substring(1);

        }
        return String.join(" ",temp);
    }
}
