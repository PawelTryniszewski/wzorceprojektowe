package WzorceStworzeniowe.Strategia;

public interface IFormatter {
    String format(String text);
}
