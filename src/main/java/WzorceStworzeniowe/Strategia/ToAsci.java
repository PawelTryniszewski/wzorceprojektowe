package WzorceStworzeniowe.Strategia;

public class ToAsci implements IFormatter {
    @Override
    public String format(String text) {
        byte[] bytes = text.getBytes();
        String[] a= new String[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            a[i]=String.valueOf(bytes[i]);
        }
        return String.join(" ",a);
    }
}
