package WzorceStworzeniowe.Strategia;

public class Main {
    public static void main(String[] args) {
        System.out.println(StringFormat(FormattingType.TOASCII,"Pawel"));
    }

    private static String StringFormat(FormattingType type,String string) {

        switch (type){

            case TOUPPER:
                return new ToUpperFormatter().format(string);

            case TOLOWWER:
                return new ToLowerFormatter().format(string);

            case CAPITAL:
                return new CapitalFormatter().format(string);

            case TOASCII:
                return new ToAsci().format(string);

            case FROMEND:
                return new FromEnd().format(string);
             default:
                 return "";
        }

    }
}

