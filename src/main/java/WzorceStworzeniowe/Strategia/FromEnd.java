package WzorceStworzeniowe.Strategia;

public class FromEnd implements IFormatter {
    @Override
    public String format(String text) {
//        String[] a = text.split("");
//        StringBuilder temp= new StringBuilder();
//        for (int i = a.length-1; i >=0 ; i--) {
//            temp.append(a[i]);
//        }
        StringBuilder stringBuilder = new StringBuilder(text);
        stringBuilder.reverse();
        return stringBuilder.toString();
    }
}
