package WzorceStworzeniowe.Builder;

public class Stamp {
    public void setFirstDayNumber(int firstDayNumber) {
        this.firstDayNumber = firstDayNumber;
    }

    public void setSecondDayNumber(int secondDayNumber) {
        this.secondDayNumber = secondDayNumber;
    }

    public void setFirstMonthNumber(int firstMonthNumber) {
        this.firstMonthNumber = firstMonthNumber;
    }

    public void setSecondMonthNumber(int secondMonthNumber) {
        this.secondMonthNumber = secondMonthNumber;
    }

    public void setYearNumber1(int yearNumber1) {
        this.yearNumber1 = yearNumber1;
    }

    public void setYearNumber2(int yearNumber2) {
        this.yearNumber2 = yearNumber2;
    }

    public void setYearNumber3(int yearNumber3) {
        this.yearNumber3 = yearNumber3;
    }

    public void setYearNumber4(int yearNumber4) {
        this.yearNumber4 = yearNumber4;
    }

    public void setCaseNumber(int caseNumber) {
        this.caseNumber = caseNumber;
    }

    @Override
    public String toString() {
        return ""+firstDayNumber + secondDayNumber +
                "-" + firstMonthNumber + secondMonthNumber +
                "-" + yearNumber1 + yearNumber2 + yearNumber3 +yearNumber4 +
                " : " + caseNumber;
    }

    //    Zadanie pieczątka.
//    Stwórz klasę Stamp która posiada pola:
    int firstDayNumber;
    int secondDayNumber;
    int firstMonthNumber;
    int secondMonthNumber;
    int yearNumber1;
    int yearNumber2;
    int yearNumber3;
    int yearNumber4;
    int caseNumber ;

    public Stamp(int firstDayNumber, int secondDayNumber, int firstMonthNumber, int secondMonthNumber, int yearNumber1, int yearNumber2, int yearNumber3, int yearNumber4, int caseNumber) {
        this.firstDayNumber = firstDayNumber;
        this.secondDayNumber = secondDayNumber;
        this.firstMonthNumber = firstMonthNumber;
        this.secondMonthNumber = secondMonthNumber;
        this.yearNumber1 = yearNumber1;
        this.yearNumber2 = yearNumber2;
        this.yearNumber3 = yearNumber3;
        this.yearNumber4 = yearNumber4;
        this.caseNumber = caseNumber;
    }
    public static class Builder{
        private int firstDayNumber;
        private int secondDayNumber;
        private int firstMonthNumber;
        private int secondMonthNumber;
        private int yearNumber1;
        private int yearNumber2;
        private int yearNumber3;
        private int yearNumber4;
        private int caseNumber;

        public Builder setFirstDayNumber() {
            this.firstDayNumber = 1;
            return this;
        }

        public Builder setSecondDayNumber() {
            this.secondDayNumber = 3;
            return this;
        }

        public Builder setFirstMonthNumber() {
            this.firstMonthNumber = 0;
            return this;
        }

        public Builder setSecondMonthNumber() {
            this.secondMonthNumber = 5;
            return this;
        }

        public Builder setYearNumber1() {
            this.yearNumber1 = 2;
            return this;
        }

        public Builder setYearNumber2() {
            this.yearNumber2 = 0;
            return this;
        }

        public Builder setYearNumber3() {
            this.yearNumber3 = 1;
            return this;
        }

        public Builder setYearNumber4() {
            this.yearNumber4 = 7;
            return this;
        }

        public Builder setCaseNumber() {
            this.caseNumber = 20;
            return this;
        }

        public Stamp createStamp() {
            return new Stamp(firstDayNumber, secondDayNumber, firstMonthNumber, secondMonthNumber,
                    yearNumber1, yearNumber2, yearNumber3, yearNumber4, caseNumber);
        }
    }
}
