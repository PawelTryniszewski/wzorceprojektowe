package WzorceStworzeniowe.Builder;

import java.util.ArrayList;
import java.util.List;

public class Main {
    GameCharacter hero = new GameCharacter.Builder().createGameCharacter();

    public static void main(String[] args) {

        List<GameCharacter> listaPostaci = new ArrayList<GameCharacter>();
        listaPostaci.add(new GameCharacter.Builder().setHealth(100).setName("Adam").createGameCharacter());
        listaPostaci.add(new GameCharacter.Builder().setHealth(100).setName("Slawek").createGameCharacter());
        listaPostaci.add(new GameCharacter.Builder().setHealth(100).setName("Daniel").createGameCharacter());

        listaPostaci.get(2).setMana(200);
        listaPostaci.get(1).setNumberOfPoints(5000);
        System.out.println(listaPostaci);
        Stamp nowa = new Stamp.Builder().setFirstDayNumber().setSecondDayNumber().setFirstMonthNumber().setSecondMonthNumber()
                .setYearNumber1().setYearNumber2().setYearNumber3().setYearNumber4().setCaseNumber().createStamp();
        System.out.println(nowa);
    }

}
