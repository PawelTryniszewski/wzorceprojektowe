package WzorceStworzeniowe.Builder.Mail;

public enum TypWiadomosci {
    UNKNOWN,
    OFFER,
    SOCIAL,
    NOTIFICATIONS,
    FORUM;
}
