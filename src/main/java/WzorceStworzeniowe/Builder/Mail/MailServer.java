package WzorceStworzeniowe.Builder.Mail;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MailServer {
    List<Client> listaKlientow = new ArrayList<>();

    public void connect(Client client) {
        listaKlientow.add(client);
    }

    public void disconnect(Client client) {
        if (listaKlientow.contains(client)) {
            listaKlientow.remove(client);
        }
    }

    public void sendMailToAll(Mail m) {
        for (Client client1 : listaKlientow) {
            m.setDataNadania(LocalDate.now());
            client1.writeMail(m);
        }
    }

    public void sendDirectMessage(Mail m, Client client) {
        for (Client client1 : listaKlientow) {
            if (client1.equals(client)) {
                m.setDataNadania(LocalDate.now());
                client1.sendDirectMessage(m);
            }
        }
    }
    public void sendWarningMesssage(String tresc) {
        for (Client client1 : listaKlientow) {
            Mail m =new Mail.Builder().createMail();
            m.setDataNadania(LocalDate.now());
            m.setNadawca("WARNING MESSAGE");
            m.setTresc(tresc);
            m.setTypWiadomosci(TypWiadomosci.FORUM);
            client1.writeMail(m);
        }
    }
    public void sendOffer(String tresc) {
        for (Client client1 : listaKlientow) {
            Mail m =new Mail.Builder().createMail();
            m.setDataNadania(LocalDate.now());
            m.setNadawca("NEW OFFER-do not reply");
            m.setTresc(tresc);
            m.setTypWiadomosci(TypWiadomosci.OFFER);
            client1.writeMail(m);
        }
    }
    public void restartAccounts() {
        for (Client client1 : listaKlientow) {
            client1.usunWszystkie();        }
    }

}
