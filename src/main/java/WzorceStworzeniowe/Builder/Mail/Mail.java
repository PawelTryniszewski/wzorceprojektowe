package WzorceStworzeniowe.Builder.Mail;

import java.time.LocalDate;

public class Mail {
    //Date data = new Date();


    @Override
    public String toString() {
        return "Mail{" +
                " nadawca='" + nadawca + '\'' +" tresc='" + tresc + '\'' +
                ", dataOdbioru=" + dataOdbioru +
                ", dataNadania=" + dataNadania +
                ", typWiadomosci=" + typWiadomosci;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }

    public void setNadawca(String nadawca) {
        this.nadawca = nadawca;
    }

    public void setDataOdbioru(LocalDate dataOdbioru) {
        this.dataOdbioru = dataOdbioru;
    }

    public void setDataNadania(LocalDate dataNadania) {
        this.dataNadania = dataNadania;
    }

    public void setAdresIp(String adresIp) {
        this.adresIp = adresIp;
    }

    public void setNazweSerweraPosredniego(String nazweSerweraPosredniego) {
        this.nazweSerweraPosredniego = nazweSerweraPosredniego;
    }

    public void setNazwaSkrzynkiPocztowej(String nazwaSkrzynkiPocztowej) {
        this.nazwaSkrzynkiPocztowej = nazwaSkrzynkiPocztowej;
    }

    public void setProtokolKomunikacji(String protokolKomunikacji) {
        this.protokolKomunikacji = protokolKomunikacji;
    }

    public void setTypWiadomosci(TypWiadomosci typWiadomosci) {
        this.typWiadomosci = typWiadomosci;
    }

    public void setCzyPrzeczytane(boolean czyPrzeczytane) {
        this.czyPrzeczytane = czyPrzeczytane;
    }

    public void setSpam(boolean spam) {
        isSpam = spam;
    }

    public String getTresc() {
        return tresc;
    }

    public String getNadawca() {
        return nadawca;
    }

    public LocalDate getDataOdbioru() {
        return dataOdbioru;
    }

    public LocalDate getDataNadania() {
        return dataNadania;
    }

    public String getAdresIp() {
        return adresIp;
    }

    public String getNazweSerweraPosredniego() {
        return nazweSerweraPosredniego;
    }

    public String getNazwaSkrzynkiPocztowej() {
        return nazwaSkrzynkiPocztowej;
    }

    public String getProtokolKomunikacji() {
        return protokolKomunikacji;
    }

    public TypWiadomosci getTypWiadomosci() {
        return typWiadomosci;
    }

    public boolean isCzyPrzeczytane() {
        return czyPrzeczytane;
    }

    public boolean isSpam() {
        return isSpam;
    }

    private String tresc;
    private String nadawca;
    private LocalDate dataOdbioru;
    private LocalDate dataNadania;
    private String adresIp;
    private String nazweSerweraPosredniego;
    private String nazwaSkrzynkiPocztowej;
    private String protokolKomunikacji;
    private TypWiadomosci typWiadomosci;
    private boolean czyPrzeczytane;
    private boolean isSpam;

    public Mail(String tresc, String nadawca, LocalDate dataOdbioru, LocalDate dataNadania, String adresIp, String nazweSerweraPosredniego, String nazwaSkrzynkiPocztowej, String protokolKomunikacji, TypWiadomosci typWiadomosci, boolean czyPrzeczytane, boolean isSpam) {
        this.tresc = tresc;
        this.nadawca = nadawca;
        this.dataOdbioru = dataOdbioru;
        this.dataNadania = dataNadania;
        this.adresIp = adresIp;
        this.nazweSerweraPosredniego = nazweSerweraPosredniego;
        this.nazwaSkrzynkiPocztowej = nazwaSkrzynkiPocztowej;
        this.protokolKomunikacji = protokolKomunikacji;
        this.typWiadomosci = typWiadomosci;
        this.czyPrzeczytane = czyPrzeczytane;
        this.isSpam = isSpam;
    }
    public static class Builder{
        private String tresc;
        private String nadawca;
        private LocalDate dataOdbioru;
        private LocalDate dataNadania;
        private String adresIp;
        private String nazweSerweraPosredniego;
        private String nazwaSkrzynkiPocztowej;
        private String protokolKomunikacji;
        private TypWiadomosci typWiadomosci;
        private boolean czyPrzeczytane;
        private boolean isSpam;

        public Builder setTresc(String tresc) {
            this.tresc = tresc;
            return this;
        }

        public Builder setNadawca(String nadawca) {
            this.nadawca = nadawca;
            return this;
        }

        public Builder setDataOdbioru(LocalDate dataOdbioru) {
            this.dataOdbioru = dataOdbioru;
            return this;
        }

        public Builder setDataNadania(LocalDate dataNadania) {
            this.dataNadania = dataNadania;
            return this;
        }

        public Builder setAdresIp(String adresIp) {
            this.adresIp = adresIp;
            return this;
        }

        public Builder setNazweSerweraPosredniego(String nazweSerweraPosredniego) {
            this.nazweSerweraPosredniego = nazweSerweraPosredniego;
            return this;
        }

        public Builder setNazwaSkrzynkiPocztowej(String nazwaSkrzynkiPocztowej) {
            this.nazwaSkrzynkiPocztowej = nazwaSkrzynkiPocztowej;
            return this;
        }

        public Builder setProtokolKomunikacji(String protokolKomunikacji) {
            this.protokolKomunikacji = protokolKomunikacji;
            return this;
        }

        public Builder setTypWiadomosci(TypWiadomosci typWiadomosci) {
            this.typWiadomosci = typWiadomosci;
            return this;
        }

        public Builder setCzyPrzeczytane(boolean czyPrzeczytane) {
            this.czyPrzeczytane = czyPrzeczytane;
            return this;
        }

        public Builder setIsSpam(boolean isSpam) {
            this.isSpam = isSpam;
            return this;
        }

        public Mail createMail() {
            return new Mail(tresc, nadawca, dataOdbioru, dataNadania, adresIp, nazweSerweraPosredniego, nazwaSkrzynkiPocztowej, protokolKomunikacji, typWiadomosci, czyPrzeczytane, isSpam);
        }
    }
}
