package WzorceStworzeniowe.Builder.Mail;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Client {
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public List<Mail> getSkrzynkaWiadomości() {
        return skrzynkaWiadomości;
    }

    public void setSkrzynkaWiadomości(List<Mail> skrzynkaWiadomości) {
        this.skrzynkaWiadomości = skrzynkaWiadomości;
    }

    List<Mail> skrzynkaWiadomości = new ArrayList<>();

    public Client(String name) {
        this.name = name;
        this.skrzynkaWiadomości = skrzynkaWiadomości;
    }

    public String getName() {
        return name;
    }

    public void sendDirectMessage(Mail mail) {
        mail.setDataNadania(LocalDate.now());
        skrzynkaWiadomości.add(mail);
    }

    public void writeMail(Mail mail) {
        mail.setDataNadania(LocalDate.now());
        skrzynkaWiadomości.add(mail);
        //System.out.println("Klient "+this.getName()+" Otrzymał maila "+mail.getTresc());
    }

    public void wyswietlWszystkieMaile() {
        for (Mail mail : getSkrzynkaWiadomości()) {
            mail.setDataOdbioru(LocalDate.now());
            System.out.println(mail);
        }if (getSkrzynkaWiadomości().size()==0) {
            System.out.println("Twoja skrzynka jest pusta.");
        }

    }

    public void wyswietlNieprzeczytane() {
        int counter = 0;
        for (Mail mail : getSkrzynkaWiadomości()) {
            if (!mail.isCzyPrzeczytane()) {
                counter++;
                mail.setDataOdbioru(LocalDate.now());
                mail.setCzyPrzeczytane(true);
                System.out.println(mail);
            }
        }
        if (counter == 0) {
            System.out.println("Nie masz nieprzeczytanych wiadomości.");
        }
    }

    public void wyswietlPrzeczytane() {

        for (Mail mail : getSkrzynkaWiadomości()) {
            if (mail.isCzyPrzeczytane()) {
                System.out.println(mail);
            }
        }
    }

    public void usunWszystkie() {
        getSkrzynkaWiadomości().removeAll(getSkrzynkaWiadomości());
    }
}
