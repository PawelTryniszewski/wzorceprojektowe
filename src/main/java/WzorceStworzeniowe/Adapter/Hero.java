package WzorceStworzeniowe.Adapter;

import java.util.Arrays;
import java.util.Iterator;

public class Hero {
    private final Iterator<Fighter> iterator;
    private Fighter[] army;

    public Hero(int sizeeOfArmy) {
        army = new Fighter[sizeeOfArmy];
        for (int i = 0; i < sizeeOfArmy; i++) {
            if (i < sizeeOfArmy / 2) {
                army[i] = new WarriorAdapter();
            } else {
                army[i] = new WarriorAdapter();
            }
        }
        iterator = Arrays.asList(army).iterator();
    }

    public void Charge() {
        for (Fighter unit : army) {
            unit.attack();
        }
    }

    public void AllWizzardsCastSpell() {
        for (Fighter unit : army) {
            if (unit instanceof WarriorAdapter) {
                unit.attack();
            }
        }
    }

    public void SendOneUnit() {
        if (iterator.hasNext()) {
            Fighter unit = (Fighter) iterator.next();
            unit.attack();
        }
    }
}
