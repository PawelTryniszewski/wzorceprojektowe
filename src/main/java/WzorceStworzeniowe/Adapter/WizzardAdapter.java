package WzorceStworzeniowe.Adapter;

public class WizzardAdapter implements Fighter {
    private final Wizard wizard;

    public WizzardAdapter(){
        this.wizard =new Wizard();

    }
    @Override
    public void attack() {
        this.wizard.castDescructionSpell();
    }

    @Override
    public void defend() {
        wizard.shiedl();
    }

    @Override
    public void escape() {
        wizard.portal();
    }
}
