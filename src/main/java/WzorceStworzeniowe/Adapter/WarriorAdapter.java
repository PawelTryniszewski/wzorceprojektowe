package WzorceStworzeniowe.Adapter;

public class WarriorAdapter implements Fighter{
    private final Warrior warrior;

    public WarriorAdapter(){
        this.warrior = new Warrior();
    }

    @Override
    public void attack() {
        this.warrior.UseSword();
        System.out.println("Wojownik musi odpocząć");
    }

    @Override
    public void defend() {
        this.warrior.Riseshiedl();
        System.out.println("Obronił króla");
    }

    @Override
    public void escape() {
        this.warrior.Run();
        this.warrior.Hide();
    }
}
